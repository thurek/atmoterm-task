# How to launch

## Start the server
* go to ```./task```
* use following command ```./gradlew build```
* afterwards launch the server locally using ```./gradlew run```

## Access app
* call endpoints on `localhost:8080/pracownicy` via `curl`