package com.atmoterm.task;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atmoterm.task.employee.AktywnyPracownik;
import com.atmoterm.task.employee.Pracownik;
import com.atmoterm.task.employee.PracownikRepository;
import com.atmoterm.task.team.Zespol;

@Configuration
public class DataLoader {

	private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

	@Bean
	CommandLineRunner initDataBase(PracownikRepository pracownikRepository) {

		Zespol zespol = new Zespol();
		zespol.setName("A-Team");

		Pracownik pracownik = new Pracownik();
		pracownik.setName("Jan Kowalski");
		pracownik.addZespol(zespol);

		AktywnyPracownik aktywnyPracownik = new AktywnyPracownik();
		aktywnyPracownik.setName("Adam Nowak");
		aktywnyPracownik.setSalary(10000D);
		aktywnyPracownik.setDataZatrudnienia(new Date());

		return args -> {
			log.info("Add: " + pracownikRepository.save(pracownik));
			log.info("Add: " + pracownikRepository.save(aktywnyPracownik));
		};

	}

}
