package com.atmoterm.task.employee;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;

@Entity
public class AktywnyPracownik extends Pracownik {

	private Double salary;

	private Date dataZatrudnienia;

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Date getDataZatrudnienia() {
		return dataZatrudnienia;
	}

	public void setDataZatrudnienia(Date dataZatrudnienia) {
		this.dataZatrudnienia = dataZatrudnienia;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		AktywnyPracownik pracownik = (AktywnyPracownik) o;
		return Objects.equals(name, pracownik.name) && Objects.equals(salary, pracownik.salary)
				&& Objects.equals(dataZatrudnienia, pracownik.dataZatrudnienia);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, salary, dataZatrudnienia);
	}

}
