package com.atmoterm.task.employee;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.atmoterm.task.team.Zespol;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(use = NAME, include = As.PROPERTY)
@JsonSubTypes({
    @JsonSubTypes.Type(value = AktywnyPracownik.class),
})
public class Pracownik {

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Employee name
	 */
	@Column(length = 50)
	protected String name;

	/**
	 * Teams the employee belongs to.
	 */
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "pracownik_zespol", joinColumns = @JoinColumn(name = "pracownik_id"), inverseJoinColumns = @JoinColumn(name = "zespol_id"))
	@JsonIgnoreProperties("pracownicy")
	private Set<Zespol> zespoly = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Zespol> getZespoly() {
		return zespoly;
	}

	public void addZespol(Zespol zespol) {
		zespoly.add(zespol);
		zespol.getPracownicy().add(this);
	}

	public void removeZespol(Zespol zespol) {
		zespoly.remove(zespol);
		zespol.getPracownicy().remove(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Pracownik pracownik = (Pracownik) o;
		return Objects.equals(name, pracownik.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

}
