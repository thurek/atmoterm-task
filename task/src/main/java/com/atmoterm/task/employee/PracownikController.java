package com.atmoterm.task.employee;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PracownikController {

	@Autowired
	private PracownikRepository pracownikRepository;

	@Autowired
	private PracownikModelAssembler pracownikModelAssembler;

	@GetMapping("/pracownicy")
	public CollectionModel<EntityModel<? extends Pracownik>> all(@RequestParam("aktywny") Optional<Boolean> aktywny) {
		List<? extends Pracownik> pracownicy;

		if (aktywny.isPresent() && aktywny.get()) {
			pracownicy = pracownikRepository.findAllAktywnyPracownik();
		} else if (aktywny.isPresent()) {
			pracownicy = pracownikRepository.findAllNieAktywnyPracownik();
		} else {
			pracownicy = pracownikRepository.findAll();
		}

		List<EntityModel<? extends Pracownik>> model = pracownicy.stream()//
				.map(pracownikModelAssembler::toModel)//
				.collect(Collectors.toList());

		return CollectionModel.of(model, linkTo(methodOn(PracownikController.class).all(null)).withSelfRel());
	}

	@GetMapping("/pracownicy/{id}")
	public EntityModel<Pracownik> one(@PathVariable Long id) {
		Pracownik pracownik = pracownikRepository.findById(id).orElseThrow(() -> new PracownikNotFoundExcpetion());
		return pracownikModelAssembler.toModel(pracownik);
	}

	@PostMapping("/pracownicy")
	public ResponseEntity<EntityModel<Pracownik>> createPracownik(@RequestBody Pracownik pracownik) {
		EntityModel<Pracownik> model = pracownikModelAssembler.toModel(pracownikRepository.save(pracownik));

		return ResponseEntity.created(model.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(model);
	}

	@PutMapping("/pracownicy/{id}")
	public ResponseEntity<EntityModel<Pracownik>> replaceEmployee(@RequestBody Pracownik incomingPracownik,
			@PathVariable Long id) {
		Pracownik updatedPracownik = pracownikRepository.findById(id).map(pracownik -> {
			pracownik.setName(incomingPracownik.getName());
			if (pracownik instanceof AktywnyPracownik && incomingPracownik instanceof AktywnyPracownik) {
				replaceActiveEmployee((AktywnyPracownik) pracownik, (AktywnyPracownik) incomingPracownik);
			}
			return pracownikRepository.save(pracownik);
		}).orElseGet(() -> {
			incomingPracownik.setId(id);
			return pracownikRepository.save(incomingPracownik);
		});

		EntityModel<Pracownik> model = pracownikModelAssembler.toModel(updatedPracownik);
		return ResponseEntity.created(model.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(model);
	}

	@DeleteMapping("/pracownicy/{id}")
	public ResponseEntity<?> deletePracownik(@PathVariable Long id) {
		pracownikRepository.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	private void replaceActiveEmployee(AktywnyPracownik aktywnyPracownik, AktywnyPracownik incomingAktywnyPracownik) {
		aktywnyPracownik.setSalary(incomingAktywnyPracownik.getSalary());
		aktywnyPracownik.setDataZatrudnienia(incomingAktywnyPracownik.getDataZatrudnienia());
	}

}
