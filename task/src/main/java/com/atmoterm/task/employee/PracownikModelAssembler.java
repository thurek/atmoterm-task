package com.atmoterm.task.employee;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class PracownikModelAssembler implements RepresentationModelAssembler<Pracownik, EntityModel<Pracownik>> {

	@Override
	public EntityModel<Pracownik> toModel(Pracownik pracownik) {
		return EntityModel.of(pracownik,
				linkTo(methodOn(PracownikController.class).one(pracownik.getId())).withSelfRel(),
				linkTo(methodOn(PracownikController.class).all(null)).withRel("pracownicy"));
	}

}
