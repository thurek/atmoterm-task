package com.atmoterm.task.employee;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PracownikNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(PracownikNotFoundExcpetion.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String employeeNotFoundHandler(PracownikNotFoundExcpetion ex) {
		return ex.getMessage();
	}

}
