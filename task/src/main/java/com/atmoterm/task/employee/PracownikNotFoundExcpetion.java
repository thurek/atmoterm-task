package com.atmoterm.task.employee;

public class PracownikNotFoundExcpetion extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -682688130135044849L;

	public PracownikNotFoundExcpetion() {
		super("Pracownik nie znaleziony!");
	}

}
