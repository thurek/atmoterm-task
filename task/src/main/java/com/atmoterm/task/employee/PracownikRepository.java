package com.atmoterm.task.employee;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PracownikRepository extends JpaRepository<Pracownik, Long> {

	
	@Query("from Pracownik p where dtype = 'AktywnyPracownik'")
	List<AktywnyPracownik> findAllAktywnyPracownik();
	
	@Query("from Pracownik p where dtype = 'Pracownik'")
	List<Pracownik> findAllNieAktywnyPracownik();
	
}
