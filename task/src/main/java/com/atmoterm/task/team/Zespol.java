package com.atmoterm.task.team;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.atmoterm.task.employee.Pracownik;

@Entity
public class Zespol {

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Team name
	 */
	@Column(length = 50)
	private String name;

	/**
	 * Teams employees
	 */
	@ManyToMany(mappedBy = "zespoly")
	private Set<Pracownik> pracownicy = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Pracownik> getPracownicy() {
		return pracownicy;
	}

	public void addPracownik(Pracownik pracownik) {
		pracownicy.add(pracownik);
		pracownik.getZespoly().add(this);
	}

	public void removePracownik(Pracownik pracownik) {
		pracownicy.remove(pracownik);
		pracownik.getZespoly().remove(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Zespol zespol = (Zespol) o;
		return Objects.equals(name, zespol.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

}
